$(function() {

    var users = [];
    var dif_username = false;
    var check = true;
    var curUserID = 0;
    
    var userUrl = 'https://us-central1-book-c35d1.cloudfunctions.net/api/users/';
    var userid = 0;
    $.get(userUrl, function(data) {
        // alert(data);
        console.log(data);
        console.log(data.length);
        users = data;
        userid = data.length;
    });

    // Handle Login
    $('#login-submit').click(function(e) {
        var uname = $('#uname').val();
        var pwd = $('#pwd').val();
        if(chkuserexist(uname, pwd)){
            alert("Hello " + uname);
            localStorage.setItem("username", uname);
            localStorage.setItem("password", pwd);
            localStorage.setItem("userid", curUserID);
            // redirect
            window.location.href = "../index.html";
        } else {
            alert("No this user or the password not correct!");
        }
    });
    
    function chkuserexist(uname, pwd){
        var exist = false;
        for (var i = 0; i < users.length; i++) {
            if (users[i].username == uname) {
                if (users[i].password == pwd){
                    curUserID = users[i].userid;
                    exist =  true;
                } else {
                    exist = false;
                }
                break;
            } else {
                exist = false;
            }
        }
        return exist;
    }

    var chkusername = $('#username').change(function() {
        var username = $('#username').val();
        if (users.length == 0) {
            dif_username = true;
        }
        else {
            for (var i = 0; i < users.length; i++) {
                console.log(users[i].username);
                if (users[i].username == username) {
                    alert("Username already taken! Please try another!");
                    dif_username = false;
                }
                else {
                    dif_username = true;
                }
            }
        }
        return dif_username;
    });

    var chkpassword = $('#password').change(function() {
        var password = $('#password').val();
        if (!password || (password.length < 1)) {
            alert('Please enter a password!');
            check = false;
        } else {
            check = true;
        }
    });
    
    var chkconfirm_password = $('#confirm_password').change(function() {
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        if (password != confirm_password) {
            alert('Confirm password and password are different!');
            check = false;
        } else {
            check = true;
        }
    });

    // Handle Register
    $('#register-submit').click(function() {
        // Ensure the input are valid
        if (chkusername && check) {
            var username = $('#username').val();
            var password = $('#password').val();
            var data = {
                userid: userid,
                username: username,
                password: password
            };
            $.post(userUrl, data, function(data, status) {
                alert("Register Status: " + status + "\nYour username: " + username + "; password: " + password);
                userid++;
                // Reset input 
                $('#userid').val(userid);
                $('#username').val("");
                $('#email').val("");
                $('#password').val("");
                $('#confirm_password').val("");
                window.location.href = "../login.html";
            });
            
        }
        else {
            alert("Register fail!");
        }
    });
});
