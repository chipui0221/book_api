var app = angular.module('myApp', []);
app.controller('DataControler', function($scope, $http) {
    var bookUrl = 'https://us-central1-book-c35d1.cloudfunctions.net/api/books/'
    var bookmarkUrl = 'https://us-central1-book-c35d1.cloudfunctions.net/api/bookmarks/';
    var userid = localStorage.getItem("userid");
    
    // Search book
    $('#search').click(function() {
        var isbn = $('#isbn').val();
        $http.get(bookUrl + isbn)
        .then(function(response){
            $scope.title = response.data.title;
            $scope.subtitle = response.data.subtitle;
            $scope.authors = response.data.authors;
            $scope.publisher = response.data.publisher;
            $scope.publishedDate = response.data.publishedDate;
            console.log(response.data);
        });
    });

    // Add book list
    $('#bookmark').click(function() {
        var isbn = $('#isbn').val();
        var comment = $('#comment').val();
        if (userid == null){
            alert("Please login first!");
            window.location.href = "../login.html";
        } else {
            $http.get(bookmarkUrl + userid)
            .then(function(response){
                if (response.data == null) {
                    //Init bookmark
                    var newBook = {
                        "books": [
                            [isbn,comment]
                        ],
                        "userid":userid
                    };
                    $http.post(bookmarkUrl,newBook)
                    .then(function successCallback(response) {
                        alert("Bookmark added successfully!");
                    }, function errorCallback(response) {
                        alert("Bookmark added fail!");
                    });
                }
                else {
                    // Add to book list
                    var bookList = [isbn,comment];
                    $http.put(bookmarkUrl + userid, bookList)
                    .then(function successCallback(response) {
                        alert("Bookmark added successfully!");
                    }, function errorCallback(response) {
                        alert("Bookmark added fail!");
                    });
                }
                console.log(response.data);
                window.location.href = "../index.html";
            });
        $('#repeatData').show();
        $('#ept').hide();
        }
    });
    
    // Show Book List
    if (localStorage.getItem("userid") != null) {
        //$('#repeatData').show();
        $('#alt').hide();
        $http.get(bookmarkUrl + userid)
            .then(function(response) { 
                if (response.data == null) {
                    $scope.bookmarks = [["Empty!"]];
                    $('#repeatData').hide();
                    $('#ept').show();
                    $('#clearBookList').hide();
                }
                else {
                    $scope.bookmarks = response.data.books;
                    console.log(response.data.books);
                    $('#ept').hide();
                }
            });
            
            
            
            
        // Handle getMoreInfo
        $scope.getMore = function(isbn) {
            if(isbn != "Empty!"){
                $http.get(bookUrl + isbn)
                    .then(function(response) { 
                        alert("Title: " + response.data.title);
                    });
            } 
        };
        
        
        
        
        
        // Handle clearBookmark
        $('#clearBookList').click(function() {
            $('#repeatData').hide();
            $http.delete(bookmarkUrl + userid)
            .then(function(response) { 
                alert(response.data.message);
                // Refresh the bookmark data
                window.location.href = "../index.html";
            });
        });
    } else {
        $('#repeatData').hide();
        $('#alt').show();
        $('#ept').hide();
        $('#clearBookList').hide();
    }
    
});
