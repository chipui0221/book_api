const functions = require('firebase-functions');
const express = require('express');
const app = express();
const cors = require('cors');
const firebase = require('firebase');
const request = require('request-promise');

app.use(cors());
var url = 'https://www.googleapis.com/books/v1/volumes?country=DK&q=isbn:';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAoQa1AULpnIBI8ZUVl3DI3ssoFWhLPah8",
    authDomain: "book-aac08.firebaseapp.com",
    databaseURL: "https://book-aac08.firebaseio.com",
    projectId: "book-aac08",
    storageBucket: "book-aac08.appspot.com",
    messagingSenderId: "1002134499610"
  };
var firebase_app = firebase.initializeApp(config);

const user_db = firebase_app.database().ref("users");
const bookmark_db = firebase_app.database().ref("bookmarks");


//Index
app.get('/', (req, res) => {
  return res.status(200).json({
    user: './users',
    book: './books/{ISBN}',
    bookmark: './bookmarks'
  });
});



// Get a book info from Google Book API by ISBN
app.get('/books/:ISBN', (req, res, next) => {
  // Get from openweather
  const isbn = req.params.ISBN.toString().trim();
  
  request({
    url: url + isbn,
    json: true
}, (error, response, body) => {
    var recivedata = {};  
    if (!error) {
      const json = body;
      if (json.totalItems) {
        recivedata = {
          title: json.items[0].volumeInfo.title,
          subtitle: json.items[0].volumeInfo.subtitle,
          authors: json.items[0].volumeInfo.authors,
          publisher: json.items[0].volumeInfo.publisher,
          publishedDate: json.items[0].volumeInfo.publishedDate
        };
      }
    }
    //return callback(return_array);
    return res.status(200).json(recivedata);
});
});



// Get all users info from firebase
app.get('/users', (req, res) => {
  return getUsers((arr) => {
    return res.status(200).json(arr);
  });
});

// Create a user to firebase
app.post('/users', (req, res) => {
  var userid = req.body.userid;
  firebase_app.database().ref('users/' + userid).set(req.body);
  return res.status(201).json({
    message: 'A new user added successfully',
    createdUser: req.body
  });
});

// Get user detail
app.get('/users/:userid', (req, res) => {
  const userid = req.params.userid;
  return getUserByID(userid, (arr) => {
    return res.status(200).json(arr);
  });
});



// Get all bookmarks info
app.get('/bookmarks', (req, res, next) => {
  return getBookmarks((arr) => {
    return res.status(200).json(arr);
  });
});

// Add a new bookmark
app.post('/bookmarks', (req, res, next) => {
  var userid = req.body.userid;
  firebase_app.database().ref('bookmarks/' + userid).set(req.body);
  return res.status(201).json({
    message: 'A new bookmark added successfully',
    createdBookmark: req.body
  });
});

// Get a bookmark info 
app.get('/bookmarks/:userid', (req, res, next) => {
  const userid = req.params.userid;
  return getBookmarksByID(userid, (arr) => {
    return res.status(200).json(arr);
  });
});

// Add a bookmark to user
app.put('/bookmarks/:userid', (req, res, next) => {
  const userid = req.params.userid;
  var bookDB = firebase_app.database().ref('bookmarks/' + userid + '/books/');
  return bookDB.once('value').then((snap) => {
    var num = snap.val().length;
    bookDB.child(num).set(req.body);
    return res.status(201).json({
      message: 'Bookmark added successfully',
      bookmarkId: req.params.userid
    });
  });
});

// Delete bookmarks of a user
app.delete('/bookmarks/:userid', (req, res, next) => {
  const userid = req.params.userid;
  firebase_app.database().ref('bookmarks/' + userid).set({});
  return res.status(200).json({
    message: 'Bookmark deleted successfully',
    bookmarkId: req.params.userid
  });
});


function getUsers(callback) {
  return user_db.once('value').then((snap) => {
    return callback(snap.val());
  });
}

function getBookmarks(callback) {
  return bookmark_db.once('value').then((snap) => {
    return callback(snap.val());
  });
}

function getUserByID(id, callback) {
  return user_db.child(id).once('value').then((snap) => {
    return callback(snap.val());
  });
}

function getBookmarksByID(id, callback) {
  return bookmark_db.child(id).once('value').then((snap) => {
    return callback(snap.val());
  });
}

function getBookByISDN(isbn, callback) {
  request({
    url: url + isbn,
    json: true
}, (error, response, body) => {
    var return_array = {};  
    if (!error && response.statusCode === 200) {
      const json = body;
      if (json.totalItems) {
        return_array = {
          title: json.items[0].volumeInfo.title,
          subtitle: json.items[0].volumeInfo.subtitle,
          authors: json.items[0].volumeInfo.authors,
          publisher: json.items[0].volumeInfo.publisher,
          publishedDate: json.items[0].volumeInfo.publishedDate
        };
      }
      
    }
    return callback(return_array);
});
}

exports.api = functions.https.onRequest(app);

